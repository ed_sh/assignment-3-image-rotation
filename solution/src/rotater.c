#include "rotater.h"
#include <stdlib.h>


inline static uint32_t get_pos_index(struct position pos, struct position size) {
    return pos.y * size.x + pos.x;
}

static struct position rotate_pos_0_times(struct position pos, struct position max_pos) {
    (void) max_pos;
    return pos;
}

static struct position rotate_pos_1_times(struct position pos, struct position max_pos) {
    return (struct position) {.x = pos.y, .y = max_pos.x - pos.x};
}

static struct position rotate_pos_2_times(struct position pos, struct position max_pos) {
    return (struct position) {.x = max_pos.x - pos.x, .y = max_pos.y - pos.y};
}

static struct position rotate_pos_3_times(struct position pos, struct position max_pos) {
    return (struct position) {.x = max_pos.y - pos.y, .y = pos.x};
}

static struct position(*rotate_by[])(struct position pos, struct position max_pos) = {
    [0] = &rotate_pos_0_times,
    [1] = &rotate_pos_1_times,
    [2] = &rotate_pos_2_times,
    [3] = &rotate_pos_3_times
};

static struct position rotate_pos(struct position pos, struct position max_pos, int times) {
    return rotate_by[times%4](pos, max_pos);
}

enum error rotate_image(struct image* img, int angle) {
    struct pixel* olddata = img->data;
    struct pixel* newdata = malloc(sizeof(struct pixel) * img->height * img->width);
    if(!newdata) return OUT_OF_MEMORY;
    
    int rot_times = (angle / 90) % 4; // rotate times by 90 degrees
    if(rot_times < 0) rot_times += 4;

    struct position max_pos = {.x = img->width-1, .y = img->height-1};

    struct position oldsize = {.x = img->width, .y = img->height};
    struct position newsize;
    
    if(rot_times % 2 != 0) { // if rotation by 90 - width swaps height
        newsize.x = oldsize.y;
        newsize.y = oldsize.x;
    } else newsize = oldsize;

    for (uint32_t x = 0; x < oldsize.x; x++) {
        for (uint32_t y = 0; y < oldsize.y; y++) {
            struct position current_pos = {.x = x, .y = y};
            struct position rotated_pos = rotate_pos(current_pos, max_pos, rot_times);
            uint32_t index = get_pos_index(current_pos, oldsize);
            uint32_t rot_index = get_pos_index(rotated_pos, newsize);
            newdata[rot_index] = olddata[index];
        }
    }

    img->width = newsize.x;
    img->height = newsize.y;
    img->data = newdata;
    free(olddata);
    return OK;
}
