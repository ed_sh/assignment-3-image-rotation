#include "filereader.h"
#include "bmpreader.h"

enum error read_bmpfile(char* filename, struct image* img) {
    FILE* fp = fopen(filename, "rb");
    if (!fp) return READ_OPEN_FILE_ERROR;

    enum error status = from_bmp(fp, img);
    fclose(fp);
    return status;
}

enum error write_bmpfile(char* filename, struct image const* img) {
    FILE* fp = fopen(filename, "wb");
    if(!fp) return WRITE_OPEN_FILE_ERROR;
    
    enum error status = to_bmp(fp, img);
    fclose(fp);
    return status;
}
