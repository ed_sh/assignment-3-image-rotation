#include "bmpreader.h"
#include <stdlib.h>

#define BM_TYPE 0x4D42
#define SIZEOF_BITMAPINFOHEADER 40
#define BI_PLANES 1
#define BITS_IN_BYTE 8

static inline uint32_t getPadding(uint32_t byteWidth) {
    return (4 - byteWidth % 4) % 4;
}

enum error from_bmp( FILE* in, struct image* img ) {
    struct bmp_header header;
    if(!fread(&header, sizeof(struct bmp_header), 1, in))
        return INVALID_PICTURE;

    img->width = header.biWidth;
    img->height = header.biHeight;

    fseek(in, header.bOffBits, SEEK_SET); // move pointer to pixel array

    uint32_t byteWidth = header.biWidth * sizeof(struct pixel);
    uint32_t padding = getPadding(byteWidth); // image width in bytes with padding

    img->data = malloc(byteWidth * header.biHeight); // prepearing data size
    if(!img->data) return OUT_OF_MEMORY;

    for (size_t i = 0; i < header.biHeight; i++) {
        if(!fread(img->data + i*header.biWidth, byteWidth, 1, in)) { // read image line
            free(img->data);
            return INVALID_PICTURE;
        }
        fseek(in, padding, SEEK_CUR); // skip padding
    }

    return OK;
}

enum error to_bmp( FILE* out, struct image const* img ) {
    uint32_t byteWidth = sizeof(struct pixel)*img->width;
    uint32_t padding = getPadding(byteWidth); // image width in bytes with padding
    char zeroes[3] = {0}; // array with 3 zeros to write padding

    struct bmp_header header = {0};
    header.bfType = BM_TYPE; // BM type
    header.bOffBits = sizeof(struct bmp_header); // pixel arr offset
    header.bfileSize = header.bOffBits + (byteWidth+padding)*img->height; // filesize is offset + arrSize with padding
    header.biSize = SIZEOF_BITMAPINFOHEADER; // sizeof BITMAPINFOHEADER
    header.biWidth = img->width;
    header.biHeight = img->height;
    header.biPlanes = BI_PLANES; // required
    header.biBitCount = sizeof(struct pixel) * BITS_IN_BYTE; // 8 bit per color
    //Undefined fields is unused and set to 0 by default

    if(!fwrite(&header, sizeof(struct bmp_header), 1, out))
        return WRITE_ERROR;

    for (size_t i = 0; i < header.biHeight; i++) {
        if(!fwrite(img->data + i*img->width, byteWidth, 1, out)) // write image line
            return WRITE_ERROR;
        fwrite(zeroes, padding, 1, out); // write padding zeroes
    }

    return OK;
}
