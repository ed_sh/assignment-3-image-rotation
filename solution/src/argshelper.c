#include "argshelper.h"

#include <stdlib.h>

enum error get_args(int argc, char** argv, struct args* args) {
    if(argc != 4) return INVALID_ARGS;
    args->in = argv[1];
    args->out = argv[2];
    
    char* angleStr = argv[3];
    int angle = atoi(angleStr); // if there is no digits return 0
    if(angle == 0 && angleStr[0] != '0') return INVALID_ARGS; // if 0 there must be '0' char

    args->angle = angle;

    return OK;
}
