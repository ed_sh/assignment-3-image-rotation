#include "argshelper.h"
#include "filereader.h"
#include "rotater.h"

int main( int argc, char** argv ) {
    struct args args;
    enum error err;
    
    err = get_args(argc, argv, &args);
    if(err) return print_err(err);

    struct image img = {0};
    err = read_bmpfile(args.in, &img);
    if(err) return print_err(err);

    err = rotate_image(&img, args.angle);
    if(err) return print_and_free(err, img.data);
    
    err = write_bmpfile(args.out, &img);
    return print_and_free(err, img.data);
}
