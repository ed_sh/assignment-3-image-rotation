#include "errors.h"

#include <stdio.h>
#include <stdlib.h>

int print_and_free(enum error err, void* to_free) {
    free(to_free);
    return print_err(err);
}


static const char* const error_defs[] = {
    [OK] = "Picture rotated successfully",
    [READ_OPEN_FILE_ERROR] = "Input file does not exists or has incorrect rights",
    [INVALID_PICTURE] = "Input file is not a valid bmp picture",
    [WRITE_ERROR] = "An error occured while saving an output picture",
    [WRITE_OPEN_FILE_ERROR] = "Output directory does not exists or has incorrect rights",
    [OUT_OF_MEMORY] = "Out of memory. Not enough space for allocating picture",
    [INVALID_ARGS] = "Invalid arguments. Usage: image-transformer <source-image> <transformed-image> <angle>"
};
int print_err(enum error err) {
    if(!err) {
        printf("%s", error_defs[err]);
        return 0;
    }

    fprintf(stderr, "Error: %s", error_defs[err]);
    return -1;
}
