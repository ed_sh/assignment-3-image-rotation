#pragma once

enum error {
  OK = 0,
  READ_OPEN_FILE_ERROR,
  INVALID_PICTURE,
  WRITE_ERROR,
  WRITE_OPEN_FILE_ERROR,
  OUT_OF_MEMORY,
  INVALID_ARGS
};

int print_err(enum error err);
int print_and_free(enum error err, void* free);
