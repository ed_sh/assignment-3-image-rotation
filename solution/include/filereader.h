#pragma once
#include "bmp.h"
#include "errors.h"

enum error read_bmpfile(char* filename, struct image* img);

enum error write_bmpfile(char* filename, struct image const* img);
