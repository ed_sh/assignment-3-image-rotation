#pragma once
#include "bmp.h"
#include "errors.h"

// Rewrites old image by rotated!
enum error rotate_image(struct image* img, int angle);

struct position {
    uint32_t x, y;
};
