#pragma once

#include "bmp.h"
#include "errors.h"
#include <stdio.h>


enum error from_bmp( FILE* in, struct image* img );

enum error to_bmp( FILE* out, struct image const* img );
