#pragma once

#include "errors.h"

struct args
{
    char* in;
    char* out;
    int angle;
};

enum error get_args(int argc, char** argv, struct args* args);
